import { useEffect, useState } from "react";
import { useParams, useNavigate } from "react-router-dom";

function SalesRecordList() {
  let { currSalesPerson } = useParams();
  const navigate = useNavigate();

  const [salesRecords, setSalesRecords] = useState([]);
  const [salesPeople, setSalesPeople] = useState([]);
  const [salesPerson, setSalesPerson] = useState("");
  const [error, setError] = useState("");
  const [success, setSuccess] = useState("");

  useEffect(() => {
    async function getSalesRecords() {
      const response = await fetch(
        "http://localhost:8090/api/sales_records/"
      ).catch(() => setError("Could not fetch sales records"));
      if (response.ok) {
        const data = await response.json();
        if (currSalesPerson > 0) {
          const newData = data.sales_records.filter(
            (salesRecord) =>
              salesRecord.sales_person.empl_id === parseInt(currSalesPerson)
          );
          setSalesRecords(newData);
        } else {
          setSalesRecords(data.sales_records);
        }
      } else {
        setError("Invalid sales record URL");
      }
    }
    async function getSalesPeople() {
      const response = await fetch(
        "http://localhost:8090/api/sales_people/"
      ).catch(() => setError("Could not fetch sales person data"));
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.sales_people);
      } else {
        setError("Invalid sales person URL");
      }
    }

    getSalesRecords();
    getSalesPeople();

    if (currSalesPerson === "true") {
      setSuccess("Sales record successfully created");
    }
  }, [salesPerson]);

  const onChange = (event) => {
    setSuccess("");
    setSalesPerson(event.target.value);
    return navigate(`/sales/sales_records/view/${event.target.value}`);
  };

  const salesRecordsList = salesRecords?.map((salesRecord) => {
    return (
      <tr key={salesRecord.id}>
        <td>{salesRecord.sales_person.empl_name}</td>
        <td>{salesRecord.sales_person.empl_id}</td>
        <td>{salesRecord.customer.cust_name}</td>
        <td>{salesRecord.automobile.vin}</td>
        <td>${salesRecord.sales_price}</td>
      </tr>
    );
  });

  const salesPersonList = salesPeople?.map((salesPerson) => {
    return (
      <option key={salesPerson.empl_id} value={salesPerson.empl_id}>
        {salesPerson.empl_name}
      </option>
    );
  });

  const currEmplName =
    currSalesPerson !== undefined
      ? salesPeople.find(
          (salesPerson) => salesPerson.empl_id === parseInt(currSalesPerson)
        )
      : null;

  const errorCheck =
    error !== "" ? (
      <div
        className="col-2 text-center alert alert-danger alert-animation"
        role="alert"
      >
        {error}
      </div>
    ) : null;
  const successCheck =
    success !== "" ? (
      <div
        className="col-2 text-center alert alert-success alert-animation"
        role="alert"
      >
        {success}
      </div>
    ) : null;
  const filterCheck =
    currSalesPerson !== undefined && currEmplName !== undefined ? (
      <h2>List of {currEmplName.empl_name}'s sales</h2>
    ) : (
      <h2>List of all sales</h2>
    );
  const emptyCheck =
    salesRecordsList.length > 0 ? null : (
      <div className="text-center alert alert-secondary m-5">
        There are no completed sales
      </div>
    );

  return (
    <>
    <div className="container">
      <div className="m-3">
        <h1 className="text-center display-3 m-5">Sales Records</h1>
        <form>
          <div className="mb-3">
            <select
              value={salesPerson}
              onChange={onChange}
              required
              id="sales-person"
              name="sales-person"
              className="form-select"
            >
              <option value="">List all sales</option>
              {salesPersonList}
            </select>
          </div>
        </form>
      </div>
      <div className="list-view text-center">
        {filterCheck}
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales person</th>
              <th>Employee ID#</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price of sale</th>
            </tr>
          </thead>
          <tbody>{salesRecordsList}</tbody>
        </table>
        {emptyCheck}
      </div>
    </div>
    <div className="mx-5">
    {errorCheck}
    {successCheck}
    </div>
    </>
  );
}

export default SalesRecordList;
