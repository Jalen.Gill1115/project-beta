from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    href = models.URLField(blank=True)


class SalesPerson(models.Model):
    empl_name = models.CharField(max_length=40)
    empl_id = models.AutoField(primary_key=True)

    def get_api_url(self):
        return reverse('api_sales_person_detail', kwargs={'empl_id': self.empl_id})

class Customer(models.Model):
    cust_name = models.CharField(max_length=40)
    cust_address = models.CharField(max_length=100)
    phone_num = models.CharField(max_length=12)

    def get_api_url(self):
        return reverse('api_customer_detail', kwargs={'id': self.id})


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
        related_name='sales_records'
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        on_delete=models.CASCADE,
        related_name='sales_records'
    )
    customer = models.ForeignKey(
        Customer,
        on_delete=models.CASCADE,
        related_name='sales_records'
    )
    sales_price = models.IntegerField(null=True)

    def get_api_url(self):
        return reverse('api_sales_record_detail', kwargs={'id': self.id})
