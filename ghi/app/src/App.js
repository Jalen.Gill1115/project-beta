import { BrowserRouter, Routes, Route } from "react-router-dom";
import MainPage from "./MainPage";
import Nav from "./Nav";
import CreateCustomer from "./sales/CreateCustomer";
import CreateSalesPerson from "./sales/CreateSalesPerson";
import CreateSalesRecord from "./sales/CreateSalesRecord";
import SalesRecordList from "./sales/SalesRecordList";
import AutomobileList from "./inventory/AutomobileList";
import CreateAutomobile from "./inventory/CreateAutomobile";
import ManufacturerList from "./inventory/ManufactuerList";
import CreateManufacturer from "./inventory/CreateManufacturer";
import VehicleModelList from "./inventory/VehicleModelList";
import CreateVehicleModel from "./inventory/CreateVehicleModel";
import TechnicianForm from "./service/TechnicianForm";
import ServiceAppointmentList from "./service/ServiceAppointmentList";
import ServiceHistory from "./service/ServiceHistory";
import ServiceAppointmentForm from "./service/ServiceAppointmentForm";

import "./css/App.css";

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path="/" element={<MainPage />} />
        <Route path="sales/">
          <Route path="customers/">
            <Route path="create/" element={<CreateCustomer />} />
          </Route>
          <Route path="sales_people/">
            <Route path="create/" element={<CreateSalesPerson />} />
          </Route>
          <Route path="sales_records/">
            <Route path="create/" element={<CreateSalesRecord />} />
            <Route path="view/" element={<SalesRecordList />}>
              <Route path=":currSalesPerson" element={<SalesRecordList />} />
            </Route>
          </Route>
        </Route>

        <Route path="inventory/">
          <Route path="automobiles/">
            <Route path="create" element={<CreateAutomobile />} />
            <Route path="view" element={<AutomobileList />} />
          </Route>
          <Route path="manufacturers/">
            <Route path="create" element={<CreateManufacturer />} />
            <Route path="view" element={<ManufacturerList />} />
          </Route>
          <Route path="vehicle_models/">
            <Route path="create" element={<CreateVehicleModel />} />
            <Route path="view" element={<VehicleModelList />} />
          </Route>
        </Route>

        <Route path="services/">
          <Route path="technicians/">
            <Route path="create" element={<TechnicianForm />} />
          </Route>
          <Route path="appointments/">
            <Route path="create" element={<ServiceAppointmentForm />} />
            <Route path="view" element={<ServiceAppointmentList />} />
            <Route path="history" element={<ServiceHistory />} />
          </Route>
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
