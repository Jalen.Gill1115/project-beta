from django.db import models

#restarted models to work with new canvas on views 1.

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

class Technician(models.Model):
    name = models.CharField(max_length=100, null=True)
    employee_id = models.IntegerField(unique=True)

class ServiceAppointment(models.Model):
    new_vin = models.CharField(max_length=17)
    name = models.CharField(max_length=200)
    date = models.DateTimeField(auto_now=False, auto_now_add=False)
    reason = models.TextField(default=False)
    technician = models.ForeignKey(
        "technician",
        related_name="appointments",
        on_delete=models.PROTECT,
        null=True
    )
    vip = models.BooleanField(default=False)
    finished = models.BooleanField(default=False)
