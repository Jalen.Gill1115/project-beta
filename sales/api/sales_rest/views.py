from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .models import AutomobileVO, Customer, SalesPerson, SalesRecord
from .encoders import AutomobileVOEncoder, CustomerEncoder, SalesRecordEncoder, SalesPersonEncoder

# Create your views here.
@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_customer_detail(request, id):
    if request.method == 'GET':
        try:
            customer = Customer.objects.get(id=id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    elif request.method == 'PUT':
        try:
            content = json.loads(request.body)
            customer = Customer.objects.get(id=id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        Customer.objects.filter(id=id).update(**content)
        customer = Customer.objects.get(id=id)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        try:
            customer = Customer.objects.get(id=id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        Customer.objects.get(id=id).delete()
        return JsonResponse(
            {"message": "Customer deleted"},
            status=200,
        )


@require_http_methods(['GET', 'POST'])
def api_customer_list(request):
    if request.method == 'GET':
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": customers},
            encoder=CustomerEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_sales_person_detail(request, id):
    if request.method == 'GET':
        try:
            sales_person = SalesPerson.objects.get(empl_id=id)
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )
    elif request.method == 'PUT':

        try:
            content = json.loads(request.body)
            sales_person = SalesPerson.objects.get(empl_id=id)
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
        SalesPerson.objects.filter(empl_id=id).update(**content)
        sales_person = SalesPerson.objects.get(empl_id=id)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

    else:
        try:
            sales_person = SalesPerson.objects.get(empl_id=id)
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
        SalesPerson.objects.get(empl_id=id).delete()
        return JsonResponse(
            {"message": "Sales person successfully deleted"},
            status=200,
        )




@require_http_methods(['GET', 'POST'])
def api_sales_person_list(request):
    if request.method == 'GET':
        sales_people = SalesPerson.objects.all()
        return JsonResponse(
            {"sales_people": sales_people},
            encoder=SalesPersonEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(
            sales_person,
            encoder=SalesPersonEncoder,
            safe=False,
        )

@require_http_methods(['GET', 'PUT', 'DELETE'])
def api_sales_record_detail(request, id):
    if request.method == 'GET':
        try:
            sales_record = SalesRecord.objects.get(id=id)
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sale record does not exist"},
                status=400,
            )
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )
    elif request.method == 'PUT':
        try:
            content = json.loads(request.body)
            sales_record = SalesRecord.objects.get(id=id)
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sale record does not exist"},
                status=400,
                safe=False,
            )
        try:
            customer = Customer.objects.get(id=content['customer'])
            sales_person = SalesPerson.objects.get(empl_id=content['sales_person'])
            automobile = AutomobileVO.objects.get(vin=content['automobile'])
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=400,
            )
        content['customer'] = customer
        content['sales_person'] = sales_person
        content['automobile'] = automobile

        SalesRecord.objects.filter(id=id).update(**content)
        sales_record = SalesRecord.objects.get(id=id)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )
    else:
        try:
            sales_record = SalesRecord.objects.get(id=id)
        except SalesRecord.DoesNotExist:
            return JsonResponse(
                {"message": "Sale record does not exist"},
                status=400,
            )
        SalesRecord.objects.get(id=id).delete()
        return JsonResponse(
            {"message": "Sale record successfully deleted"},
            status=200,
        )


@require_http_methods(['GET', 'POST'])
def api_sales_record_list(request, empl_id=None):
    if request.method == 'GET':
        if empl_id is None:
            sales_record = SalesRecord.objects.all()
        else:
            sales_record = SalesRecord.objects.filter(sales_person=empl_id)
            if not sales_record:
                return JsonResponse(
                    {"message": "This sales person has no sales record"},
                    status=400,
                )
        return JsonResponse(
            {"sales_records": sales_record},
            encoder=SalesRecordEncoder,
            safe=False,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content['automobile'])
            content["automobile"] = automobile
            customer = Customer.objects.get(id=content['customer'])
            content["customer"] = customer
            sales_person = SalesPerson.objects.get(empl_id=content['sales_person'])
            content["sales_person"] = sales_person
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Automobile does not exist"},
                status=400,
            )
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Customer does not exist"},
                status=400,
            )
        except SalesPerson.DoesNotExist:
            return JsonResponse(
                {"message": "Sales person does not exist"},
                status=400,
            )
        sales_record = SalesRecord.objects.create(**content)
        return JsonResponse(
            sales_record,
            encoder=SalesRecordEncoder,
            safe=False,
        )
