from django.urls import path
from .views import (
    api_customer_detail,
    api_customer_list,
    api_sales_person_detail,
    api_sales_person_list,
    api_sales_record_detail,
    api_sales_record_list,
)

urlpatterns = [
    path("customers/<int:id>/", api_customer_detail, name="customer_detail"),
    path("customers/", api_customer_list, name="customer_list"),
    path("sales_people/<int:id>/", api_sales_person_detail, name="sales_person_detail"),
    path("sales_people/", api_sales_person_list, name="sales_person_detail"),
    path("sales_records/<int:id>/", api_sales_record_detail, name="sales_record_detail"),
    path("sales_records/", api_sales_record_list, name="sales_record_list"),
    path("sales_records/sales_person/<int:empl_id>/", api_sales_record_list, name="sales_record_filtered_list"),
]
