import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateManufacturer() {
  const [manufacturerName, setManufacturerName] = useState("");

  const navigate = useNavigate();

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      name: manufacturerName,
    };

    const manuUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manuUrl, fetchConfig);

    if (response.ok) {
      setManufacturerName("");
      navigate("/inventory/manufacturers/view");
    }
  }
  return (
    <div className="container">
      <div className="row">
        <div className="text-center offset-2 col-9">
          <div className="shadow p-3 mt-5">
            <h1 className="offset-3 col-6">Add a new manufacturer</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  value={manufacturerName}
                  onChange={(event) => setManufacturerName(event.target.value)}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Manufacturer name</label>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default CreateManufacturer;
