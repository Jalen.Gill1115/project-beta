# CarCar

Team:

* Person 1 - Jalen Gill: Sales Microservice / Create forms for inventory
* Person 2 - Kyle / Service Microservice

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.
Starting with the manufacturer model, the input of the model took in the Charfield name, the vehicle model was then tied as a foreign key of the Manufacturer in which was able to access it through the object Manufacturer. The automobile, color, vin, year was the most crucial to inventory microservice. All of the models were intergrated within the inventory microservice views then was able to implement and access the pertinent data need in the front-end react portion.

## Sales microservice

In order to create a list of sales records, I used a few models to get all of the data I need to create one.
These models were Customer, SalesPerson, AutomobileVO, and SalesRecord. The customer and sales person are needed to show who participated in the sale, and the AutomobileVO is necessary to link existing cars to the sale record. Finally, all three of these models are needed as foreign keys in order to successfully create a sale record. The sales record contains the customer name, sales person name, automobile vin, and sales price. Every model related to sale records have complete CRUD functionality in the backend, which allows for creating, editing, deleting, and listing them as well as sale records. I also added error handling so that a JSON response will be always be returned will details on the specific error.

For the react front end, I made create forms for customers, sales people, and sales records that send post requests to the backend and create a new entry in the database for that specific model. I also added error handling here that will catch when fetching the data from the backend doesn't work due to the servers being down, and also when the URL is incorrect. If the attempt to create something was successful, I made a success message appear telling the user that it was successful. For the sale record, I had it redirect to the list of sale records after creating a successful sale record and having the success message appear on the list of it if it was redirected from there. The sale record list will can also be filtered, and will change the list based on the sales person in real time depending on the sales person selected. It also redirects to a URL containing the employee id of the sales person so that the name of the list and the list itself can be updated.

For my work in inventory, I made the create forms similar to how I created them in sales. Apon a successful creation, it will redirect to the
