import { NavLink } from "react-router-dom";

function MainPage() {
  return (
    <>
      <div
        className="px-4 py-5 my-5 text-center text-light"
        id="main-background"
      >
        <h1 className="display-1 fw-bold">AutoManage</h1>
        <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            The first stop to manage sales and service records of your auto dealership!
          </p>
        </div>
      </div>
      <div className="container-fluid">
        <div
          id="carouselExampleControls"
          class="carousel carousel-dark slide"
          data-bs-ride="carousel"
        >
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img
                src="https://www.pngmart.com/files/4/Car-Transparent-Background.png"
                class="d-block main-page-image"
                alt="..."
              />
            </div>
            <div class="carousel-item">
              <img
                src="https://www.transparentpng.com/thumb/car-png/clipart-transparent-car-7.png"
                class="d-block main-page-image"
                alt="..."
              />
            </div>
            <div class="carousel-item">
              <img src="https://www.freeiconspng.com/thumbs/car-png/red-sports-car-png-1.png" class="d-block main-page-image" alt="..." />
            </div>
          </div>
          <button
            class="carousel-control-prev"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="prev"
          >
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Previous</span>
          </button>
          <button
            class="carousel-control-next"
            type="button"
            data-bs-target="#carouselExampleControls"
            data-bs-slide="next"
          >
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="visually-hidden">Next</span>
          </button>
        </div>
      </div>
      <div className="text-center">
        <NavLink
          className="m-4 btn text-light btn-primary"
          type="button"
          to="inventory/automobiles/view"
        >
          View our inventory
        </NavLink>
        <NavLink
          className="m-4 btn text-light btn-primary"
          type="button"
          to="sales/sales_records/view"
        >
          View sales records
        </NavLink>
        <NavLink
          className="m-4 btn text-light btn-primary"
          type="button"
          to="services/appointments/view"
        >
          View service appointments
        </NavLink>
      </div>
    </>
  );
}

export default MainPage;
