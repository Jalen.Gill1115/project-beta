import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateAutomobile() {
  const [vin, setVin] = useState("");
  const [year, setYear] = useState("");
  const [color, setColor] = useState("");
  const [model, setModel] = useState("");
  const [models, setModels] = useState([]);

  const [error, setError] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const getModels = async () => {
      const response = await fetch("http://localhost:8100/api/models/").catch(
        () => setError("Could not retrieve vehicle model list")
      );
      if (response.ok) {
        const data = await response.json();
        setModels(data.models);
      } else {
        setError("Invalid response from vehicle models");
      }
    };

    getModels();
  }, []);

  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      vin: vin,
      year: year,
      color: color,
      model_id: model,
    };
    console.log(data);

    const custUrl = "http://localhost:8100/api/automobiles/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(custUrl, fetchConfig);

    if (response.ok) {
      setVin("");
      setYear("");
      setColor("");
      setModel("");
      navigate("/inventory/automobiles/view");
    } else {
      setError("Could not create new automobile");
    }
  }

  const modelList = models?.map((model) => {
    return (
      <option key={model.id} value={model.id}>
        {model.name}
      </option>
    );
  });

  const errorCheck =
    error !== "" ? (
      <div
        className="m-4 w-25 alert alert-danger offset-9 w3-animate-left"
        role="alert"
      >
        {error}
      </div>
    ) : null;

  return (
    <div className="container">
      <div className="row">
        <div className="text-center offset-2 col-8">
          <div className="shadow p-3 mt-5">
            <h1 className="offset-3 col-6">Add a new automobile</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  value={vin}
                  onChange={(event) => setVin(event.target.value)}
                  placeholder="Name"
                  required
                  maxLength="17"
                  type="text"
                  className="form-control"
                />
                <label htmlFor="name">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={year}
                  onChange={(event) => setYear(event.target.value)}
                  placeholder="Name"
                  required
                  type="text"
                  className="form-control"
                />
                <label htmlFor="name">Year</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  value={color}
                  onChange={(event) => setColor(event.target.value)}
                  placeholder="Name"
                  required
                  type="text"
                  className="form-control"
                />
                <label htmlFor="name">Color</label>
              </div>
              <div className="mb-3">
                <select
                  value={model}
                  onChange={(event) => setModel(event.target.value)}
                  required
                  className="form-select"
                >
                  <option value="">Choose a model</option>
                  {modelList}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      {errorCheck}
    </div>
  );
}

export default CreateAutomobile;
