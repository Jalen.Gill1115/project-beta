from common.json import ModelEncoder
from .models import AutomobileVO, Customer, SalesPerson, SalesRecord


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        'vin'
    ]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = [
        'id',
        'cust_name',
        'cust_address',
        'phone_num',
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = [
        'empl_name',
        'empl_id',
    ]


class SalesRecordEncoder(ModelEncoder):
    model = SalesRecord
    properties = [
        'id',
        'automobile',
        'customer',
        'sales_person',
        'sales_price',
    ]

    encoders = {
        'automobile': AutomobileVOEncoder(),
        'customer': CustomerEncoder(),
        'sales_person': SalesPersonEncoder()
    }
