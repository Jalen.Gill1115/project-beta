import React from "react";

class ServiceAppointmentForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      new_vin: "",
      name: "",
      date: "",
      time: "",
      reason: "",
      technician: "",
      technicians: [],
    };

    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInputChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({ [name]: value });
  }

  async componentDidMount() {
    const technicianURL = "http://localhost:8080/api/technician/";
    const response = await fetch(technicianURL);

    if (response.ok) {
      const data = await response.json();
      this.setState({ technicians: data.technicians });
    }
  }
  async handleSubmit(event) {
    event.preventDefault();
    const data = {
      new_vin: this.state.new_vin,
      name: this.state.name,
      date: `${this.state.date} ${this.state.time}`,
      reason: this.state.reason,
      technician: this.state.technician,
      technicians: this.state.technicians,
    };
    console.log(data);
    delete data.technicians;

    const appointmentURL = "http://localhost:8080/api/appointments/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(appointmentURL, fetchConfig);
    if (response.ok) {
      this.setState({
        new_vin: "",
        name: "",
        date: "",
        time: "",
        reason: "",
        technician: "",
      });
    }
  }

  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="header-title">Create a New Appointment</h1>
            <form onSubmit={this.handleSubmit} id="create-appointment-form">
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.new_vin}
                  placeholder="VIN"
                  required
                  type="text"
                  name="new_vin"
                  id="new_vin"
                  maxLength={17}
                  minLength={17}
                  className="form-control"
                />
                <label htmlFor="new_vin">VIN</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.name}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Customer Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.date}
                  placeholder="Date"
                  required
                  type="date"
                  name="date"
                  id="date"
                  className="form-control"
                />
                <label htmlFor="date">Date</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.time}
                  placeholder="Time"
                  required
                  type="time"
                  name="time"
                  id="time"
                  className="form-control"
                />
                <label htmlFor="time">Time</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  onChange={this.handleInputChange}
                  value={this.state.reason}
                  placeholder="Reason"
                  required
                  type="text"
                  name="reason"
                  id="reason"
                  className="form-control"
                />
                <label htmlFor="reason">Reason for Visit</label>
              </div>
              <div className="mb-3">
                <select
                  onChange={this.handleInputChange}
                  value={this.state.technician}
                  required
                  name="technician"
                  type="text"
                  className="form-select"
                >
                  <option value="">Technician</option>
                  {this.state.technicians.map((technician) => {
                    return (
                      <option
                        key={technician.employee_id}
                        value={technician.employee_id}
                      >
                        {technician.name}
                      </option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-secondary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default ServiceAppointmentForm;
