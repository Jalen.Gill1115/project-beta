from django.contrib import admin

from service_rest.models import ServiceAppointment

@admin.register(ServiceAppointment)
# Register your models here.
class ServiceAppointmentAdmin(admin.ModelAdmin):
    list_display = [
        "new_vin",
        "name",
        "date",
        "reason",
        "technician",
        "vip",
        "finished",
    ]
