import React from "react";

class TechnicianForm extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      employee_id: "",
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleInputChange(event) {
    const value = event.target.value;
    const name = event.target.name;
    this.setState({
      [name]: value,
    });
  }
  async handleSubmit(event) {
    event.preventDefault();
    const data = { ...this.state };
    console.log(data);
    const technicianURL = "http://localhost:8080/api/technician/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(technicianURL, fetchConfig);
    if (response.ok) {
      const cleared = {
        name: "",
        employee_id: "",
      };
      this.state.submit = true;
      this.setState(cleared);
    }
  }
  render() {
    return (
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1 className="header-title">Add a New Technician</h1>
            <form onSubmit={this.handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input
                  defaultValue={this.state.name}
                  onChange={this.handleInputChange}
                  placeholder="Name"
                  required
                  type="text"
                  name="name"
                  id="name"
                  className="form-control"
                />
                <label htmlFor="name">Technician Name</label>
              </div>
              <div className="form-floating mb-3">
                <input
                  defaultValue={this.state.employee_id}
                  onChange={this.handleInputChange}
                  placeholder="Employee Id"
                  required
                  type="text"
                  name="employee_id"
                  id="employee_id"
                  className="form-control"
                />
                <label htmlFor="employee_id">Employee Id</label>
              </div>
              <button className="btn btn-secondary">Add</button>
            </form>
          </div>
        </div>
      </div>
    );
  }
}

export default TechnicianForm;
