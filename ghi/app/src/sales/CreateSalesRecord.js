import React, { useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateSalesRecord() {
  const [salesPrice, setSalesPrice] = useState("");
  const [automobile, setAutomobile] = useState("");
  const [customer, setCustomer] = useState("");
  const [salesPerson, setSalesPerson] = useState("");
  const [automobiles, setAutomobiles] = useState([]);
  const [customers, setCustomers] = useState([]);
  const [salesPeople, setSalesPeople] = useState([]);

  const [error, setError] = useState("");

  const navigate = useNavigate();

  useEffect(() => {
    const getAutomobiles = async () => {
      const url = "http://localhost:8100/api/automobiles/";

      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setAutomobiles(data.autos);
      } else {
        setError("Could not find automobiles");
      }
    };

    const getCustomers = async () => {
      const url = "http://localhost:8090/api/customers/";

      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setCustomers(data.customers);
      } else {
        setError("Could not find customers");
      }
    };

    const getSalesPeople = async () => {
      const url = "http://localhost:8090/api/sales_people/";

      const response = await fetch(url);
      if (response.ok) {
        const data = await response.json();
        setSalesPeople(data.sales_people);
      } else {
        setError("Could not find sales people");
      }
    };
    getAutomobiles();
    getCustomers();
    getSalesPeople();
  }, []);
  async function handleSubmit(event) {
    event.preventDefault();
    const data = {
      sales_price: salesPrice,
      automobile: automobile,
      customer: customer,
      sales_person: salesPerson,
    };

    const salesRecordUrl = "http://localhost:8090/api/sales_records/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(salesRecordUrl, fetchConfig);

    if (response.ok) {
      navigate(`/sales/sales_records/view/${true}`);
    } else {
      setError("Could not create sales record properly.");
    }
  }

  const autoList = automobiles?.map((automobile) => {
    return (
      <option key={automobile.vin} value={automobile.vin}>
        {automobile.vin} - {automobile.model.manufacturer.name}{" "}
        {automobile.model.name}
      </option>
    );
  });

  const customerList = customers?.map((customer) => {
    return (
      <option key={customer.id} value={customer.id}>
        {customer.cust_name}
      </option>
    );
  });

  const salesPersonList = salesPeople?.map((salesPerson) => {
    return (
      <option key={salesPerson.empl_id} value={salesPerson.empl_id}>
        {salesPerson.empl_name}
      </option>
    );
  });

  const errorCheck =
    error !== "" ? (
      <div className="col-2 m-5 alert alert-danger alert-animation" role="alert">
        {error}
      </div>
    ) : null;

  return (
    <div className="container">
      <div className="row">
        <div className="text-center offset-2 col-8">
          <div className="shadow p-3 mt-5 create-form">
            <h1 className="offset-3 col-6">Add a new sale</h1>
            <form onSubmit={handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input
                  value={salesPrice}
                  onChange={(event) => setSalesPrice(event.target.value)}
                  placeholder="Name"
                  required
                  type="number"
                  name="sale"
                  className="form-control"
                />
                <label htmlFor="name">Sale Price</label>
              </div>
              <div className="mb-3">
                <select
                  value={automobile}
                  onChange={(event) => setAutomobile(event.target.value)}
                  required
                  id="automobiles"
                  name="automobiles"
                  className="form-select"
                >
                  <option value="">Choose an automobile</option>
                  {autoList}
                </select>
              </div>
              <div className="mb-3">
                <select
                  value={customer}
                  onChange={(event) => setCustomer(event.target.value)}
                  required
                  id="customers"
                  name="customers"
                  className="form-select"
                >
                  <option value="">Choose a customer</option>
                  {customerList}
                </select>
              </div>
              <div className="mb-3">
                <select
                  value={salesPerson}
                  onChange={(event) => setSalesPerson(event.target.value)}
                  required
                  id="employees"
                  name="employees"
                  className="form-select"
                >
                  <option value="">Choose a sales person</option>
                  {salesPersonList}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
      {errorCheck}
    </div>
  );
}

export default CreateSalesRecord;
