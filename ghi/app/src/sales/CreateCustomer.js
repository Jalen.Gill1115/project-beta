import React, { useState } from "react";
import { useNavigate } from "react-router-dom";

function CreateCustomer() {
  const [custName, setCustName] = useState("");
  const [custAddress, setCustAddress] = useState("");
  const [phoneNum, setPhoneNum] = useState("");

  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");

  async function handleSubmit(event) {
    event.preventDefault();
    setSuccess("");
    const data = {
      cust_name: custName,
      cust_address: custAddress,
      phone_num: phoneNum,
    };

    const custUrl = "http://localhost:8090/api/customers/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(custUrl, fetchConfig).catch(() => {
      setError("Could not communicate with server");
    });

    if (response.ok) {
      setSuccess("Customer sucessfully created");
      setCustName("");
      setCustAddress("");
      setPhoneNum("");
    } else {
      setError("Could not successfully create customer");
    }
  }

  const successCheck = success ? (
    <div
      className="col-2 text-center m-5 alert alert-success alert-animation"
      role="alert"
    >
      {success}
    </div>
  ) : null;

  const errorCheck = error ? (
    <div
      className="col-2 text-center m-5 alert alert-danger alert-animation"
      role="alert"
    >
      {error}
    </div>
  ) : null;

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="text-center offset-2 col-8">
            <div className="shadow p-3 mt-5 create-form">
              <h1 className="offset-3 col-6">Add a new customer</h1>
              <form onSubmit={handleSubmit} id="create-conference-form">
                <div className="form-floating mb-3">
                  <input
                    value={custName}
                    onChange={(event) => setCustName(event.target.value)}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Customer name</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={custAddress}
                    onChange={(event) => setCustAddress(event.target.value)}
                    placeholder="Address"
                    required
                    type="text"
                    name="address"
                    id="address"
                    className="form-control"
                  />
                  <label htmlFor="Fabric">Customer address</label>
                </div>
                <div className="form-floating mb-3">
                  <input
                    value={phoneNum}
                    onChange={(event) => setPhoneNum(event.target.value)}
                    placeholder="Phone number"
                    required
                    type="text"
                    name="phone-number"
                    id="phone-number"
                    className="form-control"
                  />
                  <label htmlFor="Color">Phone number</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      {successCheck}
      {errorCheck}
    </>
  );
}

export default CreateCustomer;
