from common.json import ModelEncoder
import json
from service_rest.models import AutomobileVO, ServiceAppointment, Technician
from datetime import datetime

class TechnicianEncoder(ModelEncoder):
    model = Technician
    properties = [
        "name",
        "employee_id",
    ]
class ScheduledEncoder(ModelEncoder):
    model = ServiceAppointment
    properties = [
        "new_vin",
        "name",
        "technician",
        "reason",
        "date",
        "vip",
        "finished",
    ]

    encoders = {
        "technician": TechnicianEncoder()
    }

    def get_extra_data(self,o):
        try:
            AutomobileVO.objects.get(vin=o.new_vin)
            return{"vip":True,}
        except:
            return {"vip":False,}


class AutomobileEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "vin",
    ]
