from django.urls import path
from .views import api_automobileVO, api_service_appointments, api_service_appointments, api_technicians, api_technician

urlpatterns = [
    path("technician/", api_technicians, name="api_technicians"),
    path("technician/<int:pk>/", api_technician, name="api_technician"),
    path("appointments/", api_service_appointments, name="api_appointments"),
    path("appointments/<int:pk>/", api_service_appointments, name="api_appointments"),
    path("automobiles/", api_automobileVO, name="api_AutomobileVO"),
]
