from django.contrib import admin
from .models import AutomobileVO

@admin.register(AutomobileVO)
# Register your models here.
class AutomobileVOAdmin(admin.ModelAdmin):
    list_display = ['vin', 'href']
