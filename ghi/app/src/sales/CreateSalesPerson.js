import React, { useState } from "react";

function CreateSalesPerson() {
  const [emplName, setEmplName] = useState("");

  const [success, setSuccess] = useState("");
  const [error, setError] = useState("");

  async function handleSubmit(event) {
    setSuccess("");
    event.preventDefault();
    const data = {
      empl_name: emplName,
    };

    const custUrl = "http://localhost:8090/api/sales_people/";
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(custUrl, fetchConfig).catch(() => {
      setError("Could not fetch sales person information")
    });

    if (response.ok) {
      setSuccess("Sales person successfully created");
      setEmplName("");
    } else {
      setError("Sales person failed to create");
    }
  }

  const successCheck = success ? (
    <div
      className="col-2 text-center m-5 alert alert-success alert-animation"
      role="alert"
    >
      {success}
    </div>
  ) : null;

  const errorCheck = error ? (
    <div
      className="col-2 text-center m-5 alert alert-danger alert-animation"
      role="alert"
    >
      {error}
    </div>
  ) : null;

  return (
    <>
      <div className="container">
        <div className="row">
          <div className="text-center offset-2 col-8">
            <div className="p-3 mt-5 create-form">
              <h1 className="offset-3 col-6">Add a new sales person</h1>
              <form onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                  <input
                    value={emplName}
                    onChange={(event) => setEmplName(event.target.value)}
                    placeholder="Name"
                    required
                    type="text"
                    name="name"
                    id="name"
                    className="form-control"
                  />
                  <label htmlFor="name">Employee name</label>
                </div>
                <button className="btn btn-primary">Create</button>
              </form>
            </div>
          </div>
        </div>
      </div>
      {successCheck}
    </>
  );
}

export default CreateSalesPerson;
