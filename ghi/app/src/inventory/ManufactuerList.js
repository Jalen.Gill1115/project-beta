import React from "react";
import { NavLink } from "react-router-dom";
class ManufacturerList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      manufacturers: [],
    };
  }

  async componentDidMount() {
    const url = "http://localhost:8100/api/manufacturers/";

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      this.setState({ manufacturers: data.manufacturers });
    }
  }
  render() {
    return (
      <div className="container list-view">
        <div>
          <h1 className="header-title-manufacturer">Manufacturers</h1>
          <div className="row row-cols-1 row-cols-md-3 g-4">
            <table className="table">
              <thead>
                <tr>
                  <th className="table-head">Name</th>
                </tr>
              </thead>
              <tbody>
                {this.state.manufacturers.map((manufacturer) => {
                  return (
                    <tr key={manufacturer.id}>
                      <td className="table-body">{manufacturer.name}</td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    );
  }
}

export default ManufacturerList;
